package com.example.patientinfo.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.example.patientinfo.entity.Category;
import com.example.patientinfo.entity.Category;


@Repository
public class CategoryRepository {
	
	
  private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	EntityManager em;
	
	public Category findById(Long id) {
		return em.find(Category.class,id);
	}
	
	
	public Category save(Category category) {
		if(category.getId() ==  null) {
			em.persist(category);
		} else {
			em.merge(category);
		}
		return category;
	}
	
	public void deleteById(Long id) {
		Category category = findById(id);
		em.remove(category);
	}
	
	
	public void findAllBasic() {
		List ResultSet = em.createQuery("select c from Category c").getResultList();
		logger.info("all Categories {} ",ResultSet);
	}
	
	public void findAllUsingTypedQuery() {
		TypedQuery<Category> query = em.createQuery("select c from Category c",Category.class);
		List<Category> allCategories = query.getResultList();
		logger.info("all categories {}",allCategories);
		
	}
	
	public void findAllUsingTypedQueryWhere() {
		TypedQuery<Category> query = em.createQuery("select c from Category c where name like f%",Category.class);
		List<Category> allCategories = query.getResultList();
		logger.info("all categories whose name starts with f {}",allCategories);
		
	}
	
	public void findAllUsingNamedquery() {
		TypedQuery<Category> query = em.createQuery("catg_no_is_30",Category.class);
		List<Category> allCategories = query.getResultList();
		logger.info("all Categorys whose category number is 30  {}",allCategories);
		
	}
	
	public void findAllUsingNamedquery2() {
		TypedQuery<Category> query = em.createQuery("catg_starts_with_d",Category.class);
		List<Category> allCategories = query.getResultList();
		logger.info("all Categories whose category name starts with d {}",allCategories);
		
	}
	
	public void findAllUsingNativequery() {
		Query query = em.createQuery("select * from Category_details",Category.class);
		List<Category> allCategories = query.getResultList();
		logger.info("all Categorys {}",allCategories);
		
	}


}
