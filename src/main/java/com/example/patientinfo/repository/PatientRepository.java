package com.example.patientinfo.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.example.patientinfo.entity.Patient;

@Repository
public class PatientRepository {
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	EntityManager em;
	
	//basic operations
	public Patient findById(Long id) {
		return em.find(Patient.class,id);
	}
	
	
	public Patient save(Patient patient) {
		if(patient.getId() ==  null) {
			em.persist(patient);
		} else {
			em.merge(patient);
		}
		return patient;
	}
	
	public void deleteById(Long id) {
		Patient patient = findById(id);
		em.remove(patient);
	}
	
	//queries
	public void findAllBasic() {
		List ResultSet = em.createQuery("select p from Patient p").getResultList();
		logger.info("all patients {} ",ResultSet);
	}
	
	public void findAllUsingTypedQuery() {
		TypedQuery<Patient> query = em.createQuery("select p from Patient p",Patient.class);
		List<Patient> allPatients = query.getResultList();
		logger.info("all patients {}",allPatients);
		
	}
	
	public void findAllUsingTypedQueryWhere() {
		TypedQuery<Patient> query = em.createQuery("select p from Patient p where name like f%",Patient.class);
		List<Patient> allPatients = query.getResultList();
		logger.info("all patients whose name starts with f {}",allPatients);
		
	}
	
	public void findAllUsingNamedquery() {
		TypedQuery<Patient> query = em.createQuery("catg_no_is_30",Patient.class);
		List<Patient> allPatients = query.getResultList();
		logger.info("all patients whose category number is 30  {}",allPatients);
		
	}
	
	public void findAllUsingNamedquery2() {
		TypedQuery<Patient> query = em.createQuery("catg_starts_with_d",Patient.class);
		List<Patient> allPatients = query.getResultList();
		logger.info("all patients whose category name starts with d {}",allPatients);
		
	}
	
	public void findAllUsingNativequery() {
		Query query = em.createQuery("select * from patient_details",Patient.class);
		List<Patient> allPatients = query.getResultList();
		logger.info("all patients {}",allPatients);
		
	}
	
	 /*public void findAllUsingNativeQueryPositionalParameters(String name) {
		 Query query = em.createNativeQuery("select * from patient_details name=?",Patient.class);
		 query.setParameter(1, name);
		 List allPatients = query.getResultList();
		logger.info("all patients {}",allPatients);
	}

	public void findAllUsingNativeQueryNamedParameters(Long name) {
		Query query = em.createNativeQuery("select * from patient_details where patient_name= :patient_name",Patient.class);
		query.setParameter("name", name);
		List allPatients = query.getResultList();
		logger.info("all patients {}",allPatients);
		}*/
	
	
	
	

}
