package com.example.patientinfo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.example.patientinfo.repository.CategoryRepository;
import com.example.patientinfo.repository.PatientRepository;

@SpringBootApplication
public class PatientinfoApplication implements CommandLineRunner {
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	PatientRepository patRepository;
	
	@Autowired
	CategoryRepository catgRepository;
	
	public static void main(String[] args) {
		
		
		SpringApplication.run(PatientinfoApplication.class, args);
	}
	
	@Override
	public void run(String... args) throws Exception {	
		
		//patRepository.findAllBasic();
		//patRepository.findAllUsingTypedQuery();
		//patRepository.findAllUsingTypedQueryWhere();
		//patRepository.findAllUsingNamedquery();
		//patRepository.findAllUsingNamedquery2();
		//patRepository.findAllUsingNativequery();
		//patRepository.findAllUsingNativeQueryPositionalParameters(10L);
		//patRepository.findAllUsingNativeQueryNamedParameters(30L);
		
		//catgRepository.findAllBasic();
		//catgRepository.findAllUsingTypedQuery();
		//catgRepository.findAllUsingTypedQueryWhere();
		//catgRepository.findAllUsingNamedquery();
		//catgRepository.findAllUsingNamedquery2();
		//catgRepository.findAllUsingNativequery();
		//catgRepository.findAllUsingNativeQueryPositionalParameters("hyd");
		//catgRepository.findAllUsingNativeQueryNamedParameters("blr");
		
	}

}
