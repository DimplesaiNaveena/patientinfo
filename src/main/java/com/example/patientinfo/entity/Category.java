package com.example.patientinfo.entity;

import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

@Entity
@Table(name = "category_table")
public class Category {
	
	@Id
	@GeneratedValue
	private Long id;
	
	private String catgName;
	private String catgLocation;
	
	@CreationTimestamp
	private LocalDateTime createdDateTime;
	
	@UpdateTimestamp
	private LocalDateTime updatedDateTime;

	public Category() {
	
	}

	public Category(String catgName, String catgLocation) {
		super();
		this.catgName = catgName;
		this.catgLocation = catgLocation;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCatgName() {
		return catgName;
	}

	public void setCatgName(String catgName) {
		this.catgName = catgName;
	}

	public String getCatgLocation() {
		return catgLocation;
	}

	public void setCatgLocation(String catgLocation) {
		this.catgLocation = catgLocation;
	}
	
	@Override
	public String toString() {
		return String.format("category name=[%s] location=[%s]",catgName,catgLocation);
	}
}
